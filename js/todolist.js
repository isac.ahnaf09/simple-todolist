const taskList = [
    {
        id : 1,
        taskName : "Doing my homework", 
        status : true
    },
    {
        id : 2,
        taskName : "Completing Chapter 4 Binar Academy", 
        status : true
    },
    {
        id : 3,
        taskName : "Go to the next Chapter", 
        status : false
    },
];

function addTask(){
    let addTaskListValue = document.getElementById('addTaskInput').value;
    let button = document.getElementById('buttonAdd').textContent;

    
    if(addTaskListValue){
       if(button == "Add"){
        //add the new value
        let newId = randomIdGenerator();
        
        //tmp is a temporary variable that holds the value of user input
        let tmp = {
            id : newId,
            taskName : addTaskListValue,
            status : false
        }
        
        //push the variable/object tmp to array taskList
        taskList.push(tmp)

        //reset input value
        document.getElementById('addTaskInput').value = '';


        //display the new list
        fetchAll(); 
       }
    }
}

function fetchAll(){
        var data = '';
        let tableTask = document.getElementById('tableTaskList')
    
        if(taskList.length >= 0){
            for(i = 0; i < taskList.length; i++){
                data += '<tr>';

                if(taskList[i].status){
                    data += '<td><input id="checkboxTask" name="checkboxTask" type="checkbox" checked onclick="statusUpdateTask('+ i +')"/> <label id="labelTask'+ i +'" class="strikethrough"> ' + taskList[i].taskName + '</label> </td>';
                }else{
                    data += '<td><input id="checkboxTask" name="checkboxTask" type="checkbox" onclick="statusUpdateTask('+ i +')"/> <label id="labelTask'+ i +'" class=" "> ' + taskList[i].taskName + '</label> </td>';
                }
                data += '<td><button class = "edit btn-special"onclick="updateTask(' + i + ')"></button></td>';
                data += '<td><button class = "delete btn-special"onclick="deleteTask(' + i + ')"></button></td>';
                data += '</tr>';
            }
        }   
        return tableTask.innerHTML = data;
}

function randomIdGenerator(){
    // let id = Math.floor(Math.random() * 100);  
    let id = taskList.length;
    id++;
    return id;
}

function deleteTask(id){        
    taskList.splice(id, 1);

    fetchAll();
}

function updateTask(id){
    let updateTaskList = document.getElementById('updateTask')

    updateTaskList.value = taskList[id].taskName;

    //set display text field pada class addField and updateField
    document.getElementById('updateField').style.display = 'block';
    let addField = document.getElementById('addField')
    addField.classList.add("displayInput")

    document.getElementById('saveUpdate').onsubmit = function(){

        let taskUpdated = updateTaskList.value;

        if(taskUpdated){
            //set value from taskUpdated to taskList
            taskList[id].taskName = taskUpdated;

            //display all taskList
            self.fetchAll()
            closeUpdateField();
        }
    }
}

function closeUpdateField (){
    document.getElementById('updateField').style.display = 'none';
    
    let addField = document.getElementById('addField')
    addField.classList.remove("displayInput")
}

function statusUpdateTask(id){
    let labelTask = document.getElementById('labelTask'+id);

    let statusBeforeUpdate = taskList[id].status

    if (statusBeforeUpdate === true){
        taskList[id].status = false
        labelTask.classList.remove('strikethrough')
        console.log(taskList)
    }else{
        labelTask.classList.add('strikethrough')
        taskList[id].status = true
        console.log(taskList)
    }

}

fetchAll();